const BASE_URL = "https://api.openbrewerydb.org/breweries";
import { notifier } from '@beyonk/svelte-notifications';

class Api {
    list(options = {}, fetchContainer = window) {
        if (options == null) {
            options = {};
        }
        let url = `${BASE_URL}`;
        const urlParams = [];
        if (options.by_city)  urlParams.push(`by_city=${options.by_city}`);
        if (options.by_name)  urlParams.push(`by_name=${options.by_name}`);
        if (options.by_type)  urlParams.push(`by_type=${options.by_type}`);
        if (options.by_tag)   urlParams.push(`by_tag=${options.by_tag}`);
        if (options.by_tags)  urlParams.push(`by_tags=${options.by_tags}`);
        if (options.by_state) urlParams.push(`by_state=${options.by_state}`);
        if (options.sort)     urlParams.push(`sort=${options.sort}`);

        urlParams.push("per_page=1000");

        const urlParamString = urlParams.join('&');
        url = `${url}?${urlParamString}`;
        return fetchContainer.fetch(url)
                             .then(response => response.json())
                             .catch(() => {
                                notifier.danger("Looks like you're offline!");
                                return [];
                             });
    }

    get(id, fetchContainer = window) {
        return fetchContainer.fetch(`${BASE_URL}/${id}`)
                             .then(response => response.json())
                             .catch(() => {
                                notifier.danger("Looks like you're offline!");
                                return null;
                             });
    }
}

export default new Api();
